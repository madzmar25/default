# Default Laravel Project

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

This project contains default laravel project with nothing on it. As of creation of this project Laravel is on version 5.4.

Together with this project I have added ansible as a default provisioner. This contains LEMP stack like

##### Packages:
- Ansible (Host on the vagrant box not on local machine)
- Git
- Nginx
- MySQL
- PHP5
    - mcrypt
    - php5-cli
    - php5-curl
    - php5-fpm
    - php5-intl
    - php5-json
    - php5-mcrypt
    - php5-sqlite
    - sqlite3

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

You can learn ansible from this documentation [Ansible Docs](http://docs.ansible.com/).

Here is also a link that teaches the very basic use of [Ansible](https://adamcod.es/2014/09/23/vagrant-ansible-quickstart-tutorial.html)

## Author
####Madzmar A. Ullang

[Bitbucket](https://bitbucket.org/madzmar25/) | [Github](https://github.com/ramzdam) | [LinkedIn](https://www.linkedin.com/in/madzmar-ullang-41883292?trk=nav_responsive_tab_profile)






